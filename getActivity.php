<?php
$token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNjEzZTJiZWI4YTM4ZjgyNWMxNzMzMWY4ZGIxMGQ5MWNkMGExZjQxZjY2MWMyZmM3NWI2MmJmMmM4Yzk5NDUxNzMxZjE0NTA3YTViZjhmNTEiLCJpYXQiOjE2NjA3MTE1MjEuOTc2MTM0LCJuYmYiOjE2NjA3MTE1MjEuOTc2MTM5LCJleHAiOjQ4MTYzODUxMjEuOTcyMjU5LCJzdWIiOiIyMzk1NiIsInNjb3BlcyI6WyJlbWFpbF9mdWxsIiwiZG9tYWluc19mdWxsIiwiYWN0aXZpdHlfZnVsbCIsImFuYWx5dGljc19mdWxsIiwidG9rZW5zX2Z1bGwiLCJ3ZWJob29rc19mdWxsIiwidGVtcGxhdGVzX2Z1bGwiLCJzdXBwcmVzc2lvbnNfZnVsbCIsInNtc19mdWxsIiwiZW1haWxfdmVyaWZpY2F0aW9uX2Z1bGwiXX0.J33ltn-DavYzxetqNev-kVZaa8RoIND092cR7uoWzDERsblRUQg-pz80IF3joOxLX1zbdPlOM1VDcz7m6V3w2kkA73DT2jBjKXCeqc4z--7TWshOBFRUJXKMsJEM1l7CnOf1H1VeBL0wUhERaJtPwaK211_91TzCUIFR-WvHg0PdOFmvAC_NtuuBVUgOJbU1OF6LZ1s3TrT2hPvpzBf7YW_JgFn-qA8trJFLVZpRokWNW_KE704BOvG1RDmvHY9MkeBZ-VGtN2swSEMIKQ6VGbVon2uyZyqFPi9HVipFy4pWxPoFhR46z2d30APsMTL3uQ4PI_-YmGYPJKlzO-EJYUyMnBePnEgOfXJjevfvAjUqMI6FKimlZKttjJcCEss2C8CB-aJY3IaMT6oX9OueNJe0NiQ_KkAml0k6Mn0F3P-Te_WnqrBRIX-MwL6cHCBYQcKgp6Qzwktvr7Kz_4nOwsSS3wOQBksT6Qc-QFKkAj4LRt7oxzAcxQ5_r0Lrl5_moeHR7SBxTp7FFD1h-ZGNZVrMSp6lghBqXzftHKUWDQ0VoP6wn4bNod0ZHyZQUYvBOlkTUEAV_D_D_FrjelvKEWUHqL4FxK6hmowhxyGqIe0GXm98g4BQ8oZ-dRR9XzUne0gRvOhm8GoXWEXOgrqU4MqcLc66UoRA1liTzJxdCzk';
$domainId = 'ynrw7gympvrg2k8e';
$from_email = strtolower($_GET['from']);
$to_email = strtolower($_GET['to']);
if (isset($_GET['date'])) {
    $from_date = $_GET['date'];
    //$to_date = $_GET['to_date'];
    $date_from = strtotime($from_date);
    $date_to = strtotime($from_date . ' +1 day');
} else {
    $date_from = '';
    $date_to = '';
}
$data = [];
$limit = 100;
for ($page = 1; $page < 50; $page++) {
    $url = "https://api.mailersend.com/v1/activity/$domainId?date_from=$date_from&date_to=$date_to&limit=$limit&page=$page&from_email=$from_email";
    header('Content-Type: application/json'); // Specify the type of data
    $ch = curl_init($url); // Initialise cURL
    $authorization = "Authorization: Bearer " . $token; // Prepare the authorisation token
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization)); // Inject the token into the header
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($ch, CURLOPT_POST, 1); // Specify the request method as POST
    //curl_setopt($ch, CURLOPT_POSTFIELDS, $post); // Set the posted fields
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
    $result = curl_exec($ch); // Execute the cURL statement
    curl_close($ch); // Close the cURL connection
    //echo $result;
    $emails = json_decode($result, true);
    if (count($emails['data']) == 0) {
        break;
    }
    foreach ($emails['data'] as $email) {
        if (!empty($from_email)) {
            if ($email['email']['from'] == $from_email) {
                if (!empty($to_email)) {
                    if ($email['email']['recipient']['email'] == $to_email) {
                        $data[] = $email;
                    }
                } else {
                    $data[] = $email;
                }
            }
        } else {
            if (!empty($to_email)) {
                if ($email['email']['recipient']['email'] == $to_email) {
                    $data[] = $email;
                }
            } else {
                $data[] = $email;
            }
        }
    }
}
echo json_encode($data);

