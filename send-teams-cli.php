<?php

if (count($argv) < 5) {
    echo "Usage: send-teams-cli.php [Title] [SubTitle] [Message] [Hook URL] [Icon URL (optional)]\n";
    die;
}

$title = $argv[1];
$subtitle = $argv[2];
$message = $argv[3];
$hook = $argv[4];
if (isset($argv[5])) {
    $imageUrl = $argv[5];
} else {
    $imageUrl = 'https://doleminhtri.com/images/common/headshot.jpg' ;
}

echo json_encode(sendTeams($title, $subtitle , [
    [
        'name'  => 'Message',
        'value' =>  "$message"
    ]
], $hook, $imageUrl));

function sendTeams($title, $subtitle, $facts, $hook, $imageUrl)
{
    $data = [
        "@type" => "MessageCard",
        "@context" => "http://schema.org/extensions",
        "themeColor" => "0076D7",
        "summary" => $title,
        "sections" => [
            [
                "activityTitle" => $title,
                "activitySubtitle" => $subtitle,
                "activityImage" => $imageUrl,
                "facts" => $facts,
                "markdown" => true
            ]
        ]
    ];

    $data_string = json_encode($data);

    $ch = curl_init($hook);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
    );

    return curl_exec($ch);
}