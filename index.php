<?php
error_reporting(E_ERROR | E_PARSE);

$noSends = [
    'notification@clevver.io',
    'mail@clevver.io'
];

require "bootstrap.php";
include "send-teams.php";

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode( '/', $uri );

// all of our endpoints start with /person
// everything else results in a 404 Not Found

$secret = getenv("SECRET_URL");

if ($uri[1] !== $secret) {
    header("HTTP/1.1 404 Not Found");
    echo json_encode([
        'result'    =>  false,
        'message'   =>  "Not authorize"
    ]);
    exit();
}

// SEND TEAM NOTIFICATION
if (getenv('TEAM_HOOK')) {
    $hook = getenv('TEAM_HOOK');
} else {
    $hook = 'https://dieholding.webhook.office.com/webhookb2/c1226a3c-b336-4bff-bea7-e088a6c00733@48cc99a5-4aa1-4afc-b0f5-4aab1c191752/IncomingWebhook/0a230c317b4f44b2bbf2270d6eb1124d/84fc8051-0094-45da-ba66-92fe87d71c5e';
}

if (getenv('IMAGE_URL')) {
    $imageUrl = getenv('IMAGE_URL');
} else {
    $imageUrl = 'https://doleminhtri.com/images/common/headshot.jpg' ;
}

// the user id is, of course, optional and must be a number:
$userId = null;
if (isset($uri[2])) {
    $userId = (int) $uri[2];
}

$requestMethod = $_SERVER["REQUEST_METHOD"];

// pass the request method and user ID to the PersonController and process the HTTP request:
$input = (array) json_decode(file_get_contents('php://input'), TRUE);

$date = date("[Y-m-d H:i:s] ");
file_put_contents('data.txt', $date.file_get_contents('php://input').PHP_EOL, FILE_APPEND);
echo $date.file_get_contents('php://input');

//$input = (array) json_decode(file_get_contents('data.json'), TRUE);
$id = $input['data']['email']['id'];
$created_at = $input['data']['email']['created_at'];
$subjectEmail = $input['data']['email']['subject'];
$from = $input['data']['email']['from'];
$recipient = $input['data']['email']['recipient']['email'];
$failedMessage = $input['data']['morph']['object'] . ' - ' . $input['data']['morph']['reason'];

$subject = "Email Bounced - ID: $id";
$content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>'.$subject.'</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
<h3>Hello '.$from.'</h3>
<p>Your email was not sent successfully via MailerSend.</p>
<table>
<tr>
    <td style="color: darkred; padding-right: 10px;">Subject:</td>
    <td>'.$subjectEmail.'</td>
</tr>
<tr>
    <td style="color: darkred; padding-right: 10px;">To: </td> 
    <td>'.$recipient.'</td>
</tr>
<tr>
    <td style="color: darkred; padding-right: 10px;">Sent At: </td>
    <td>'. $created_at.'</td>
</tr>
<tr>
    <td style="color: darkred; padding-right: 10px;">Error: </td> 
    <td>'.$failedMessage.'</td>
</tr>
</table>
</body>
</html>
';

if (!filter_var($from, FILTER_VALIDATE_EMAIL)) {
    header("HTTP/1.1 400 Wrong Email");
    echo json_encode([
        'result'    =>  false,
        'message'   =>  "Data is invalid"
    ]);
    $content = "FROM IP: " . get_client_ip() . " </br>";
    $content .= getRequestHeaders() . "</br>";
    $content .= file_get_contents('php://input');
    //sendMail('[Email Bounced] - Data is invalid', $content,'tri@clevver.io');
    sendTeams("Bounced Email", "Invalid Data", [
        [
            'name'  => 'IP',
            'value' =>  get_client_ip()
        ],
        [
            'name'  => 'Headers',
            'value' =>  getRequestHeaders()
        ],
        [
            'name'  => 'Data',
            'value' =>  file_get_contents('php://input')
        ]
    ], $imageUrl, $hook);
    exit();
}
// SENT TO USER
if (!in_array($from, ["MAILER-DAEMON@mail.clevvermail.com", "notification@clevver.io", "mail@clevver.io"])) {
    sendMail($subject, $content, $from);
}

sendTeams("Bounced Email", "ID: $id", [
    [
        'name'  => 'From',
        'value' =>  "$from"
    ],
    [
        'name'  => 'To',
        'value' =>  "$recipient"
    ],
    [
        'name'  => 'Subject',
        'value' =>  "$subjectEmail"
    ],
    [
        'name'  => 'Message',
        'value' =>  "$failedMessage"
    ],
    [
        'name'  => 'Sent At',
        'value' =>  "$created_at"
    ]
], $imageUrl, $hook);

echo json_encode([
    'result'    =>  true,
    'message'   =>  "Sent email $subject to $from"
]);

function sendMail($subject, $content, $to)
{
    global $noSends;
    if (in_array($to, $noSends)) {
        return "Not send $to.";
    }
    $smtp = getenv("SMTP_HOST");
    $username = getenv("USERNAME");
    $password = getenv("PASSWORD");
    $from = getenv("SMTP_FROM");


    // Create the Transport
    $transport = (new Swift_SmtpTransport($smtp, 465, 'ssl'))
        ->setUsername($username)
        ->setPassword($password)
        ->setStreamOptions(array('ssl' => array('allow_self_signed' => true, 'verify_peer' => false)));

    // Create the Mailer using your created Transport
    $mailer = new Swift_Mailer($transport);

    // Create a message
    $message = (new Swift_Message($subject))
        ->setFrom([$from])
        ->setTo( [$to] )
        ->setBody($content,  'text/html');
    // Send the message
    return $mailer->send($message);
}
